"""Huffmans coding"""

import sys


class Tree:
    def __init__(self, node):
        self.root_node = node

    def get_root_node(self):
        return self.root_node


class Node:
    def __init__(self):
        self.value = None
        self.character = None
        self.left_node = None
        self.right_node = None

    def get_value(self):
        return self.value

    def get_char(self):
        return self.character

    def get_left_node(self):
        return self.left_node

    def get_right_node(self):
        return self.right_node

    def set_value(self, value):
        self.value = value

    def set_char(self, charecter):
        self.character = charecter

    def set_left_node(self, node):
        self.left_node = node

    def set_right_node(self, node):
        self.right_node = node

    def has_left_node(self):
        return bool(self.left_node)

    def has_right_node(self):
        return bool(self.right_node)


def get_char_freq(data):
    all_freq = {}

    for i in data:
        if i in all_freq:
            all_freq[ i ] += 1
        else:
            all_freq[ i ] = 1
    return all_freq


def sort_queue(data):
    return sorted(data, key=lambda x: x[ 1 ])


def set_value_node(value, left_node, right_node):
    new_node = Node()
    new_node.character = "ND"
    new_node.set_value(value)
    new_node.set_right_node(right_node)
    new_node.set_left_node(left_node)
    return new_node


def create_node(data):
    data_node = Node()
    data_node.set_value(data[ 1 ])
    data_node.set_char(data[ 0 ])
    return data_node


def add_data_to_node(data1, data2, root_node):
    add_values = data1[ 1 ] + data2[ 1 ]
    left_node = Node()
    left_node.set_value(data1[ 1 ])
    left_node.set_char(data1[ 0 ])
    right_node = Node()
    right_node.set_value(data2[ 1 ])
    right_node.set_char(data2[ 0 ])
    root_node.set_char("ND")
    root_node.set_value(add_values)
    root_node.set_left_node(left_node)
    root_node.set_right_node(right_node)

    return root_node


def huffman_tree(data):
    root_node = None
    while len(data):

        data = sorted(data, key=lambda x: x[ 1 ])
        next_data = data.pop(0)
        if not root_node:
            root_node = Node()
            data1 = next_data
            data2 = data.pop(0)
            if data1[ 1 ] > data2[ 1 ]:
                dummy_data = data1
                data1 = data2
                data2 = dummy_data
            root_node = add_data_to_node(data1, data2, root_node)
            continue
        if root_node.value <= next_data[ 1 ]:
            if type(next_data[ 0 ]) == str:
                right_node = create_node(next_data)
            else:
                right_node = next_data[ 0 ]
            add_values = root_node.get_value() + right_node.get_value()

            root_node = set_value_node(add_values, root_node, right_node)
            continue

        elif root_node.value and root_node.value > next_data[ 1 ] and len(data) > 0:
            data1 = next_data
            data2 = data.pop(0)
            if type(data1[ 0 ]) == str:
                node_1 = create_node(data1)
            else:
                node_1 = data1[ 0 ]

            if type(data2[ 0 ]) == str:
                node_2 = create_node(data2)
            else:
                node_2 = data2[ 0 ]
            add_values = node_1.get_value() + node_2.get_value()

            new_node = set_value_node(add_values, node_1, node_2)
            new_data = (new_node, add_values)
            data.append(new_data)
            continue

        if len(data) == 0:
            previous_node = root_node
            if type(next_data[ 0 ]) == str:
                child_node = create_node(next_data)
            else:
                child_node = next_data[ 0 ]
            add_values = child_node.value + previous_node.value

            if child_node.value <= previous_node.value:
                node = set_value_node(add_values, child_node, previous_node)
            else:
                node = set_value_node(add_values, previous_node, child_node)
            root_node = node
    return root_node



def tree_traversal(root):
    code = {}

    if root is None:
        return None
    encoded_data = str()

    def recursive(node, encoded_data, code):
        if node:
            if node.has_left_node():
                raw_data = "0"
                encoded_data += raw_data
                if node.left_node.character != "ND":
                    code[ node.left_node.character ] = encoded_data
                code = recursive(node.left_node, encoded_data, code)
            if node.has_right_node():
                raw_data = "1"
                encoded_data = encoded_data[ :-1 ]
                encoded_data += raw_data
                if node.right_node.character != "ND":
                    code[ node.right_node.character ] = encoded_data
                code = recursive(node.right_node, encoded_data, code)
        return code

    data = recursive(root, encoded_data, code)
    return data


def node_check(node):
    if node:
        node_check(node.left_node)
        print(node.character)
        node_check(node.right_node)


def sentence_to_code(code, sentence):
    output = str()
    for letter in sentence:
        output += code[ letter ]

    return output


def huffman_encoding(sentence):
    if not sentence:
        print("Invalid Sentence")
        return None, None
    char_data = get_char_freq(sentence)
    data = sorted(char_data.items(), key=lambda x: x[ 1 ])
    root = huffman_tree(data)
    encrypted_data = tree_traversal(root)


    return sentence_to_code(encrypted_data, sentence), root


def huffman_decoding(data, tree):
    if not data or not tree:
        print("Invalid Inputs")
        return None
    root_tree = tree
    decoded_data = str()

    for bit in data:
        if bit == "0":
            tree = tree.left_node
        if bit == "1":
            tree = tree.right_node
        try:
            if tree.left_node.character == "ND" and tree.right_node.character == "ND":
                pass
        except AttributeError:
            decoded_data += tree.character
            tree = root_tree

    return decoded_data

if __name__ == "__main__":
    codes = {}

    a_great_sentence = "The bird is the word"

    print("The size of the data is: {}\n".format(sys.getsizeof(a_great_sentence)))
    print("The content of the data is: {}\n".format(a_great_sentence))

    encoded_data, tree = huffman_encoding(a_great_sentence)

    print("The size of the encoded data is: {}\n".format(sys.getsizeof(int(encoded_data, base=2))))
    print("The content of the encoded data is: {}\n".format(encoded_data))

    decoded_data = huffman_decoding(encoded_data, tree)

    print("The size of the decoded data is: {}\n".format(sys.getsizeof(decoded_data)))
    print("The content of the encoded data is: {}\n".format(decoded_data))

    # Add your own test cases: include at least three test cases
    # and two of them must include edge cases, such as null, empty or very large values

    # Test Case 1
    print(f"TEST CASE 1:")
    a_great_sentence = "          GOOD"

    print("The size of the data is: {}\n".format(sys.getsizeof(a_great_sentence)))
    print("The content of the data is: {}\n".format(a_great_sentence))

    encoded_data, tree = huffman_encoding(a_great_sentence)

    print("The size of the encoded data is: {}\n".format(sys.getsizeof(int(encoded_data, base=2))))
    print("The content of the encoded data is: {}\n".format(encoded_data))

    decoded_data = huffman_decoding(encoded_data, tree)

    print("The size of the decoded data is: {}\n".format(sys.getsizeof(decoded_data)))
    print("The content of the encoded data is: {}\n".format(decoded_data))
    print(f"Is Input Content and decoded same: ------ {decoded_data == a_great_sentence}")

    # Test Case 2

    print(f"TEST CASE 2:")
    a_great_sentence = "Generating random paragraphs can be an excellent way for writers to get their creative flow going at the beginning of the day. The writer has no idea what topic the random paragraph will be about when it appears. This forces the writer to use creativity to complete one of three common writing challenges. The writer can use the paragraph as the first one of a short story and build upon it. A second option is to use the random paragraph somewhere in a short story they create. The third option is to have the random paragraph be the ending paragraph in a short story. No matter which of these challenges is undertaken, the writer is forced to use creativity to incorporate the paragraph into their writing."

    print("The size of the data is: {}\n".format(sys.getsizeof(a_great_sentence)))
    print("The content of the data is: {}\n".format(a_great_sentence))

    encoded_data, tree = huffman_encoding(a_great_sentence)

    print("The size of the encoded data is: {}\n".format(sys.getsizeof(int(encoded_data, base=2))))
    print("The content of the encoded data is: {}\n".format(encoded_data))

    decoded_data = huffman_decoding(encoded_data, tree)

    print("The size of the decoded data is: {}\n".format(sys.getsizeof(decoded_data)))
    print("The content of the encoded data is: {}\n".format(decoded_data))
    print(f"Is Input Content and decoded same: ------ {decoded_data == a_great_sentence}")

    # Test Case 3

    print(f"TEST CASE 3:")
    a_great_sentence = ""
    print("The content of the data is: {}\n".format(a_great_sentence))

    encoded_data, tree = huffman_encoding(a_great_sentence)

    decoded_data = huffman_decoding(encoded_data, tree)
