import hashlib
import datetime


class Block:

    def __init__(self, data, previous_hash):
        self.timestamp = datetime.datetime.utcnow()
        self.data = data
        self.previous_hash = previous_hash
        self.hash = self.calc_hash()
        self.next = None

    def calc_hash(self):
        sha = hashlib.sha256()

        hash_str = self.data.encode('utf-8')

        sha.update(hash_str)

        return sha.hexdigest()


class BlockChain:
    def __init__(self):
        self.head = None
        self.tail = None

    def add_block_to_chain(self, data):
        if data is None or data == "":
            return
        elif self.head is None:
            self.head = Block(data, 0)
            self.tail = self.head
        else:
            self.tail.next = Block(data, self.tail.hash)
            self.tail = self.tail.next
        return

    def to_list(self):
        out = [ ]
        block = self.head
        while block:
            out.append([ block.data, block.hash ])
            block = block.next
        return out


# Test Case 1
block = BlockChain()
data1 = "ABC"
data2 = "DEF"
data3 = "GHI"
block.add_block_to_chain(data1)
block.add_block_to_chain(data2)
block.add_block_to_chain(data3)
print(block.to_list())  # prints block chain
# Test Case 2
block2 = BlockChain()
block2.add_block_to_chain("")
block2.add_block_to_chain("")
print(block2.to_list())  # prints empty block chain as there was no data passed
# Test Case 3
block = BlockChain()
block.add_block_to_chain(None)
block.add_block_to_chain(None)
print(block.to_list())  # prints empty block chain as there was no data passed

