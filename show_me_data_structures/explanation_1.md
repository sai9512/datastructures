
* Data Structures Used - List, Dict.
* Hash table is stored in Dict as key and value.
* Main values of cache are stored in list making sure the list is not exceeding the cache capacity.


The choice of Dict and array Datastructures is to full-fill O(1) time complexity.

time complexity O(capacity) might occur when setting the value where cache capacity is filled and need to be replaced, since the hashtable is finding the least used key and replacing with new keys.


