class Node:
    def __init__(self, value):
        self.value = value
        self.next = None

    def __repr__(self):
        return str(self.value)


class LinkedList:
    def __init__(self):
        self.head = None

    def __str__(self):
        cur_head = self.head
        out_string = ""
        while cur_head:
            out_string += str(cur_head.value) + " -> "
            cur_head = cur_head.next
        return out_string


    def append(self, value):

        if self.head is None:
            self.head = Node(value)
            return

        node = self.head
        while node.next:
            node = node.next

        node.next = Node(value)

    def size(self):
        size = 0
        node = self.head
        while node:
            size += 1
            node = node.next

        return size

    def to_set(self):
        output = set()
        node = self.head
        while node:
            output.add(node.value)
            node = node.next
        return output

def union(llist_1, llist_2):
    # Your Solution Here
    set_1 = llist_1.to_set()
    set_2 = llist_2.to_set()
    union = set_1.union(set_2)
    if len(union) >0:
        return union
    return

def intersection(llist_1, llist_2):
    # Your Solution Here
    set_1 = llist_1.to_set()
    set_2 = llist_2.to_set()
    intersection = set_1.intersection(set_2)
    if len(intersection) >0:
        return intersection
    return


# Test case 1

linked_list_1 = LinkedList()
linked_list_2 = LinkedList()

element_1 = [3,2,4,35,6,65,6,4,3,21]
element_2 = [6,32,4,9,6,1,11,21,1]

for i in element_1:
    linked_list_1.append(i)

for i in element_2:
    linked_list_2.append(i)

print("Test Case 1")
print (f"Union: {union(linked_list_1,linked_list_2)}")
print (f"Intersection: {intersection(linked_list_1,linked_list_2)}")

# Test case 2

linked_list_3 = LinkedList()
linked_list_4 = LinkedList()

element_1 = [3,2,4,35,6,65,6,4,3,23]
element_2 = [1,7,8,9,11,21,1]

for i in element_1:
    linked_list_3.append(i)

for i in element_2:
    linked_list_4.append(i)

print("Test Case 2")

print (f"Union: {union(linked_list_3,linked_list_4)}")
print (f"Intersection: {intersection(linked_list_3,linked_list_4)}")

# Add your own test cases: include at least three test cases
# and two of them must include edge cases, such as null, empty or very large values

# Test Case 3
print("Test Case 3")

print (f"Union: {union(LinkedList(),linked_list_4)}")
print (f"Intersection: {intersection(LinkedList(),linked_list_4)}")

# Test Case 4
print("Test Case 4")

print (f"Union: {union(LinkedList(),LinkedList())}")
print (f"Intersection: {intersection(LinkedList(),LinkedList())}")