Huffman Encoding:
Description:

Values in tree are sorted. Each tree leaf is considered as a linked list and sorted by value.

runtime:

Sorted : O(n.log n)
Huffman_tree: O(m * n logn)
tree_traversal: O(m)

Huffman Decoding: o(n):
A for loop is used to go through all the nodes and decode the characters.
O(m)




