"""Finding Files"""
import os

"""
Python's os module will be useful—in particular, you may want to use the following resources:

os.path.isdir(path)

os.path.isfile(path)

os.listdir(directory)

os.path.join(...)"""


def find_files(suffix, path, list_of_paths):
    """
    Find all files beneath path with file name suffix.

    Note that a path may contain further subdirectories
    and those subdirectories may also contain further subdirectories.

    There are no limit to the depth of the subdirectories can be.

    Args:
      suffix(str): suffix if the file name to be found
      path(str): path of the file system

    Returns:
       a list of paths
       :param list_of_paths:
    """

    if os.path.isdir(path):
        list_in_directory = os.listdir(path)
        if list_in_directory:
            for files in list_in_directory:
                file_path = os.path.join(path, files)
                if os.path.isfile(file_path):
                    if file_path.endswith(suffix):
                        list_of_paths.append(file_path)
                else:
                    find_files(suffix, file_path, list_of_paths)

    return list_of_paths


# Add your own test cases: include at least three test cases
# and two of them must include edge cases, such as null, empty or very large values

# Test Case 1
paths = find_files(".c", "./testdir", [])
print(paths)
print("expected", ['./testdir/t1.c', './testdir/subdir1/a.c', './testdir/subdir3/subsubdir1/b.c', './testdir/subdir5/a.c'])

# Test Case 2
paths_2 = find_files(".h", "./testdir", [])
print(paths_2)
print("expected",['./testdir/subdir1/a.h', './testdir/t1.h', './testdir/subdir3/subsubdir1/b.h', './testdir/subdir5/a.h'])
# Test Case 3
paths_3 = find_files(".gitkeep", "./testdir", [])
print(paths_3)
print("expected",['./testdir/subdir4/.gitkeep', './testdir/subdir2/.gitkeep'])

# Test Case 4
paths_4 = find_files(".p", "./testdir", [])
print(paths_4)
print("expected",[])