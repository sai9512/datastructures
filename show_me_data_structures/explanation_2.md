* Recursive function is used, the fuctionality is same finding file with required suffix in a directry
* same function is called recursively until the file is found and the paths are returned.
* if requested file is not available then the function returns []
* Time complexity is O(n)