"""LRU Cache"""


class LRUCache(object):

    def __init__(self, capacity):
        # Initialize class variables'
        self.capacity = capacity
        # initialize dict to store hash values
        self.hash_values = {}
        # initialize Bucket array
        self.array = []
        # Initialize sorting list through hash values
        self.key_usage = [i for i in range(self.capacity)]

    def hash_table(self, key):

        if not self.array:
            index = 0
        elif len(self.array) < self.capacity:
            index = len(self.array)
            if index in self.key_usage:
                self.key_usage.pop(index)
            self.key_usage = [index] + self.key_usage
        else:
            index = self.key_usage.pop(-1)
            self.key_usage = [index] + self.key_usage

            for k, v in list(self.hash_values.items()):
                if index == v:
                    del self.hash_values[k]

        self.hash_values[key] = index
        return index

    def get(self, key):

        # Retrieve item from provided key. Return -1 if nonexistent.
        if key in self.hash_values.keys():
            index = self.hash_values[key]
            value = self.array[index]
            if index in self.key_usage:
                self.key_usage.remove(index)

            self.key_usage = [index] + self.key_usage

            return value
        else:
            return -1

    def set(self, key, value):
        # Set the value if the key is not present in the cache.
        # If the cache is at capacity remove the oldest item.
        index = self.hash_table(key)

        if len(self.array) <= index:
            self.array.append(value)
        elif len(self.array) > index:
            self.array[index] = value


our_cache = LRUCache(5)

our_cache.set(1, 1);
our_cache.set(2, 2);
our_cache.set(3, 3);
our_cache.set(4, 4);

print("result should be: 1", our_cache.get(1))  # returns 1
print("result should be: 2", our_cache.get(2))  # returns 2
print("result should be: -1", our_cache.get(9))  # returns -1 because 9 is not present in the cache

our_cache.set(5, 5)
our_cache.set(6, 6)

print("result should be: -1"
      , our_cache.get(3))  # returns -1 because the cache reached it's capacity and 3 was the least recently used entry

# Add your own test cases: include at least three test cases
# and two of them must include edge cases, such as null, empty or very large values

# Test Case 1
our_cache.set("a", "b")
print("result should be: b", our_cache.get("a"))
# Test Case 2
our_cache.set(100000, 0)
print("result should be: 0", our_cache.get(100000))

# Test Case 3
our_cache.set(1, 100000)
print("result should be: 100000", our_cache.get(1))
